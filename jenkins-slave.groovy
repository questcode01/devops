podTemplate(
  name: 'questcode',
  namespace: 'devops',
  label: 'questcode',
  containers: [
    containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'docker', livenessProbe: containerLivenessProbe(execArgs: '', failureThreshold: 0, initialDelaySeconds: 0, periodSeconds: 0, successThreshold: 0, timeoutSeconds: 0), name: 'docker-container', resourceLimitCpu: '', resourceLimitMemory: '', resourceRequestCpu: '', resourceRequestMemory: '', ttyEnabled: true)
  ],
  volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')],
) {
  // PIPELINE
  node('questcode') {
    stage('Build') {
        echo 'Iniciando Clone do Repositorio'
        sh 'ls -ltra'
    }
    stage('Package') {
        container('docker-container') {
          echo 'Iniciando Build com npm'
          sh 'docker images'
          sh 'ls -ltra'
        }
    }
    stage('Deploy') {
        echo 'Iniciando Deploy com Helm'
        sh 'ls -ltra'
    }
  }
}
